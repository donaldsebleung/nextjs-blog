/**
 * @type {import('next').NextConfig}
 */
const CI_PROJECT_NAME = process.env.CI_PROJECT_NAME || false
const isGitLabCI = CI_PROJECT_NAME

const nextConfig = {
  basePath: isGitLabCI ? `/${CI_PROJECT_NAME}` : '',
  images: {
    unoptimized: true,
  },
}

module.exports = nextConfig
